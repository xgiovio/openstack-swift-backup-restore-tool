__author__ = 'xgiovio'

import swift_generate_temp_url

secretkey =""
set_secretkey = False
create_temp_url = True
duration_in_seconds = 60*60*2
objectpath = "#####"    #  container/object
fail_tries = 30


swift_generate_temp_url.launch(secretkey,set_secretkey,create_temp_url,duration_in_seconds,objectpath,fail_tries)