__author__ = 'xgiovio'

import authentication, swiftclient
import hmac,sys
from hashlib import sha1
from time import time

def launch(secretkey,set_secretkey,create_temp_url,duration_in_seconds,objectpath,fail_tries):

    swift_conn = authentication.set_authentication ()
    storageurl,_ = swift_conn.get_auth()
    #print(storageurl)
    for fail_tries_counter in range (fail_tries) :
        try:
            if set_secretkey:
                swift_conn.post_account({"x-account-meta-temp-url-key":secretkey})
            else:
                headers = swift_conn.head_account()
                secretkey = headers['x-account-meta-temp-url-key']
        except Exception as e:
            print("Exception during setting / getting the secret key.")
            print(e)
            time.sleep(2)
            if fail_tries_counter == fail_tries - 1 :
                print("Maximum tries reached. Exiting.")
                sys.exit(-1)
            else:
                swift_conn = authentication.set_authentication ()
        else :
            break
    print("Secretkey " + secretkey)
    if create_temp_url :
        storageurl = storageurl.replace("https://","")
        method = 'GET'
        expires = int(time() + duration_in_seconds)
        path = "/" + storageurl.split("/")[1] + "/" + storageurl.split("/")[2] + "/" + objectpath
        key = secretkey
        hmac_body = '%s\n%s\n%s' % (method, expires, path)
        sig = hmac.new(key.encode("utf-8"), hmac_body.encode("utf-8"), sha1).hexdigest()
        s = 'https://{host}{path}?temp_url_sig={sig}&temp_url_expires={expires}'
        url = s.format(host=storageurl.split("/")[0], path=path, sig=sig, expires=expires)

        print(url)

