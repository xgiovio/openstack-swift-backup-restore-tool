__author__ = 'xgiovio'

import  local_to_swift
size_limit_to_segment = 2147483648 #must be a power of 2 # def 2147483648
# 2MB 2097152
# 4MB 4194304
# 8MB 8388608
# 128MB 134217728
# 256MB 268435456
# 512MB 536870912
# 1GB 1073741824
# 2GB 2147483648
# 4GB 4294967296
size_limit_reading_os = 134217728 #must be a power of 2 and smaller/equal than size_limit_to_segment #  def 134217728
# 64k 65536
# 128k 131072
# 256k 262144
# 512k 524288
# 1MB 1048576
# 2MB 2097152
# 4MB 4194304
# 8MB 8388608
# 128MB 134217728
# 256MB 268435456
# 512MB 536870912
# 1GB 1073741824
# 2GB 2147483648
# 4GB 4294967296
upload = True
enableLarge = True
fail_tries = 30
temp_path = "\\\\?\\" + "c:\\temp\\"
excluded_patterns = ["Thumbs.db",".DS_Store","_gsdata_","__MACOSX", "desktop.ini","@eaDir"]
delete_excluded_patterns = ["!CB_"]
batch = [
            #source, swift container, swift prefix, md5 comparison enabled?, encrypted?, encryption_key, additional_excluded_patterns,copy_to_dir,delete_inexistent_remote_files,additional_delete_excluded_patterns

            ["\\\\?\\" + "c:\\test\\","default","test/",False,True,"ciao",[],None,True,[]] ,
            ["\\\\?\\" + "c:\\test\\","default","test/",False,True,"ciao",[],None,True,[]] ,
        ]

###############################################################################################
for job in batch:
    #local folder,temp path, swift container, swift prefix, size to segment, size reading limit os, upload enabled?. upload large enabled? , fail tries, md5 comparison enabled?, encrypted?, encryption_key,additional_excluded_patterns,copy_to_dir, delete_inexistent_remote_files,additional_delete_excluded_patterns
    local_to_swift.launch(job[0],temp_path,job[1],job[2],size_limit_to_segment,size_limit_reading_os,upload,enableLarge,fail_tries, job[3],job[4],job[5], job[6] + excluded_patterns,job[7],job[8],job[9] + delete_excluded_patterns)