__author__ = 'xgiovio'

import  swift_to_local
size_limit_reading_os = 134217728 #must be a power of 2 and smaller/equal than size_limit_to_segment #  def 134217728
# 64k 65536
# 128k 131072
# 256k 262144
# 512k 524288
# 1MB 1048576
# 2MB 2097152
# 4MB 4194304
# 8MB 8388608
# 128MB 134217728
# 256MB 268435456
# 512MB 536870912
# 1GB 1073741824
# 2GB 2147483648
# 4GB 4294967296
download = True
fail_tries = 30
excluded_patterns = ["Thumbs.db",".DS_Store","_gsdata_","__MACOSX", "desktop.ini","@eaDir","!CB_"]
batch = [
            #source, swift container, swift prefix, md5 comparison enabled?, encrypted?, encryption_key, additional_excluded_patterns
            ["\\\\?\\" + "C:\\test\\","default","test/",False,True,"ciao",[]] ,

        ]

###############################################################################################
for job in batch:
    #local folder,temp path, swift container, swift prefix, size to segment, size reading limit os, upload enabled?. upload large enabled? , fail tries, md5 comparison enabled?, encrypted?, encryption_key
    swift_to_local.launch(job[0],job[1],job[2],size_limit_reading_os,download,fail_tries, job[3],job[4],job[5], job[6] + excluded_patterns)