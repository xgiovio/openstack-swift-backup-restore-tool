__author__ = 'xgiovio'

#######################################authentication
authentication="v1"
#######################################types of authentication
#authentication="v1"
swift_user = "########"
swift_pass = "########"
swift_auth = "https://www.########"
#authentication="pre"
url = "https://########/v1/AUTH_###############"
tok ="########"
#######################################end authentication
timeout = 10  #sec
insecure = False


import swiftclient

def set_authentication ():
    if authentication=="v1":
        swift_conn = swiftclient.client.Connection(authurl=swift_auth, user=swift_user, key=swift_pass, timeout = timeout,insecure = insecure)
        print("Using v1 authentication")
    elif authentication=="pre":
        swift_conn = swiftclient.client.Connection(preauthurl= url,preauthtoken=tok,timeout = timeout,insecure = insecure)
        print("Using pre authentication")
    else:
        #v1 again
        swift_conn = swiftclient.client.Connection(authurl=swift_auth, user=swift_user, key=swift_pass,timeout = timeout,insecure = insecure)
        print("Using v1/alternative authentication")
    return swift_conn
