__author__ = 'xgiovio'



import authentication, utility

def launch(swift_container,delete,fail_tries):
    print ("Swift container " + swift_container)
    print ("Delete " + str(delete))
    print ("Fail tries " + str(fail_tries))
    print("___________")

    swift_conn = authentication.set_authentication ()

    listfoldermanifest=[]
    #all files on the container
    results = utility.get_list(fail_tries,swift_conn,swift_container,"")
    if not results:
        return
    swift_conn,container_objects_raw = results
    #get dict name:manifesturl only for large files
    byte0manifest,swift_conn,container_objects_manifest = utility.list_compute_manifest (fail_tries,container_objects_raw,swift_conn,swift_container,"")
    for name,manifest in container_objects_manifest.items():
        listfoldermanifest.append(manifest)
    '''
    for o in listfoldermanifest:
        print(o)
    '''
    #_____________________________________________________________________________________________________________________
    #______________________________Folders with segments
    listfoldersegmentsprefix = []
    listfoldersegmentsprefixfull = {}

    #______________________________Folders with segments         container: swift_container +"_segments"

    #all files on container_segments -> they are only segments
    results = utility.get_list(fail_tries,swift_conn,swift_container +"_segments","")
    if not results :
        return
    swift_conn,container_segments_objects_raw = results
    #get dict name:size
    container_segments_objects = utility.list (container_segments_objects_raw,"")
    #get list only with folders path name from segments
    listfolder = []

    for name in container_segments_objects.keys():
        if (swift_container +"_segments/" + utility.folder_from_path(name,"/")) not in listfoldersegmentsprefixfull.keys():
            listfoldersegmentsprefixfull[swift_container +"_segments/" + utility.folder_from_path(name,"/")]= []
        listfoldersegmentsprefixfull[swift_container +"_segments/" + utility.folder_from_path(name,"/")].append(swift_container +"_segments/" + name)

        if utility.folder_from_path(name,"/") not in listfolder :
            listfolder.append(utility.folder_from_path(name,"/"))
    for o in listfolder:
        listfoldersegmentsprefix.append(swift_container +"_segments/" + o)

    #______________________________Folders with segments         container: swift_container path: @SynologyCloudSync/
    #all files @SynologyCloudSync/  on container -> they are only segments
    #get dict name:size
    container_segments_objects = utility.filter_list_begin(container_objects_raw,"@SynologyCloudSync/","")
    #get list only with folders path name from segments
    listfolder = []
    for name in container_segments_objects.keys():
        if (swift_container +"/" + utility.folder_from_path(name,"/")) not in listfoldersegmentsprefixfull.keys():
            listfoldersegmentsprefixfull[swift_container +"/" + utility.folder_from_path(name,"/")]=[]
        listfoldersegmentsprefixfull[swift_container +"/" + utility.folder_from_path(name,"/")].append(swift_container +"/" + name)
        if utility.folder_from_path(name,"/") not in listfolder :
            listfolder.append(utility.folder_from_path(name,"/"))
    for o in listfolder:
        listfoldersegmentsprefix.append(swift_container +"/" + o)


    #______________________________Folders with segments           container: swift_container path: files with !CB_
    #all files !CB_ of CloudBerry  on container -> they are only segments
    #get dict name:size
    container_segments_objects = utility.search_list(container_objects_raw,"!CB_","")
    #get list only with folders path name from segments
    listfolder = []
    for name in container_segments_objects.keys():
        if (swift_container +"/" + utility.folder_from_path(name,"/")) not in listfoldersegmentsprefixfull.keys():
            listfoldersegmentsprefixfull[swift_container +"/" + utility.folder_from_path(name,"/")]=[]
        listfoldersegmentsprefixfull[swift_container +"/" + utility.folder_from_path(name,"/")].append(swift_container +"/" + name)
        if utility.folder_from_path(name,"_") not in listfolder :
            listfolder.append(utility.folder_from_path(name,"_"))
    for o in listfolder:
        listfoldersegmentsprefix.append(swift_container +"/" + o)


    #_____________________________________________________________________________________________________________________
    '''
    for o in listfoldersegmentsprefix:
        print(o)
    '''
    #_____________________________________________________________________________________________________________________

    print("___________")
    segments_to_delete = []
    segments_not_listed = []
    for manifesturlsegments in listfoldersegmentsprefix :
        if manifesturlsegments not in listfoldermanifest:
            segments_to_delete.append(manifesturlsegments)

    print("Segments folders to delete: " + str(len(segments_to_delete)))
    for manifest in segments_to_delete:
        print("Segments folder to delete: " + manifest)

    print("___________")
    if delete:
        errors_deleting_files = 0
        skipped_files = 0
        for short,longlist in listfoldersegmentsprefixfull.items():
            if short in segments_to_delete:
                for long in longlist:
                    del_container = long.split("/")[0]
                    del_object =  long.replace(del_container + "/","")
                    swift_conn,errors_deleting_file,_,skipped_file,_ = utility.delete_object(swift_conn,del_container,del_object,None,fail_tries)
                    errors_deleting_files += errors_deleting_file
                    skipped_files += skipped_file
        print("___________")
        print("Errors deleting files " + str(errors_deleting_files))
        print("Skipped files to delete " + str(skipped_files))
    else:
        print("Delete Disabled")

    print("___________")
    #_____
    for manifesturl in listfoldermanifest :
        if manifesturl not in listfoldersegmentsprefix:
            segments_not_listed.append(manifesturl)

    for manifest in segments_not_listed:
        print("Segments not present in given segments list : " + manifest)

    #_____




    swift_conn.close()
