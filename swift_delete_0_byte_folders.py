
import authentication, utility

def launch(swift_container,delete,fail_tries):

    print ("Swift container " + swift_container)
    print ("Delete " + str(delete))
    print ("Fail tries " + str(fail_tries))
    print("___________")
    swift_conn = authentication.set_authentication()
    results = utility.get_list(fail_tries,swift_conn,swift_container,"")
    if not results:
        return
    swift_conn,objects = results
    remotefiles = utility.list_compute_0_byte_folders(objects)


    for file in remotefiles:
        print("0 byte directory to delete: " + file )
    print("___________")
    print("Directories to delete " + str(len(remotefiles)) )

    if delete and len(remotefiles) > 0:
        errors_deleting_files = 0
        skipped_files = 0
        for file in remotefiles:
            swift_conn,errors_deleting_file,_,skipped_file,_ = utility.delete_object(swift_conn,swift_container,file,None,fail_tries)
            errors_deleting_files += errors_deleting_file
            skipped_files += skipped_file
        print("___________")
        print("Errors deleting files " + str(errors_deleting_files))
        print("Skipped files to delete " + str(skipped_files))
    else:
        if not delete and len(remotefiles) > 0:
            print("Delete Disabled")
    print("___________")
    print("___________")
